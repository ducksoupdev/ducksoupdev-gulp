(function () {
    'use strict';

    var msnry;
    var currentPage = 1;
    var totalPages = 1;

    function removeClass(el, className) {
        if (el.classList) {
            el.classList.remove(className);
        } else {
            el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
        }
    }

    function initialize() {
        // remove the js class
        removeClass(document.querySelector('body'), 'no-js');

        // set-up masonry if on home or tag pages
        if (document.querySelector('.js-posts-container')) {
            imagesLoaded('.js-posts-container', function() {
                msnry = new Masonry(document.querySelector('.js-posts-container'), {
                    itemSelector: '.post-stub',
                    hiddenStyle: {opacity: 0},
                    visibleStyle: {opacity: 1},
                    transitionDuration: '0.5s'
                });
            });
        }

        // the load more button
        var loadMorePosts = document.querySelector('.js-load-more-posts');
        if (loadMorePosts) {
            var href = loadMorePosts.dataset.href;
            if (href) {
                totalPages = parseInt(loadMorePosts.dataset.totalPages, 10);
                loadMorePosts.addEventListener('click', function(ev) {
                    ev.preventDefault();
                    atomic.get(href + '/' + (currentPage + 1) + '/index.html')
                        .success(function(data, xhr) {
                            // add pages to masonry and trigger the layout
                            var newItemsContainer = document.createElement('div');
                            var container = document.querySelector('.js-posts-container');
                            newItemsContainer.innerHTML = data;

                            var newElements = newItemsContainer.querySelectorAll('.post-stub');
                            console.log(newElements);
                            for (var i = 0; i < newElements.length; i++) {
                                container.appendChild(newElements[i]);
                            }

                            msnry.addItems(newElements);
                            msnry.layout();

                            currentPage++;
                            if (totalPages === currentPage) {
                                loadMorePosts.style.display = 'none';
                            }
                        })
                        .error(function(data, xhr) {
                            console.error('Unable to load additional pages');
                        });
                    return false;
                });
            }
        }
    }

    document.addEventListener('DOMContentLoaded', initialize);
})();
