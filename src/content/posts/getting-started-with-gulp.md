---
slug: getting-started-with-gulp
title: Getting started with Gulp
template: post.hbs
date: 2015-01-10
author: Matt Levy
tags: tooling gulp
---

![](/images/gulp.jpg)

If you are looking to use GulpJS as your build tool of choice and don't know where to start,
this guide will get you up and running in no time.

## What this quick guide isn't

This quick guide is not a Grunt v Gulp comparison. Both Grunt and Gulp are excellent tools for
reducing the amount of repetitive tasks a developer runs daily. Personally, I use both for
different projects.

## Installation

First you need to install gulp globally so you have the command line tool.

    $ npm install -g gulp

Next, you need to install it as part of your project. This is so you can use the API when creating tasks.

    $ npm init
    $ npm install --save-dev gulp

## Gulpfile.js

Once you have gulp installed, you need to create the gulp file which is suitably named Gulpfile.js (inline with other
famous build files like Makefile, Rakefile.rb, Gruntfile.js etc).

After you have created the Gulpfile.js, the easiest task to get you started is the default one. Copy and paste the following
code to your Gulpfile.js

    var gulp = require('gulp');

    gulp.task('default', function() {
        return console.log('Hello, gulp!');
    });

This uses the Gulp API `gulp.task` to create a task to run.

## Running tasks

To run your new task, simple type the following command:

    $ gulp

You should then see the following output from Gulp:

    [20:52:54] Using gulpfile ~/source/gulp-starter/Gulpfile.js
    [20:52:54] Starting 'default'...
    Hello, gulp!
    [20:52:54] Finished 'default' after 1 ms

Running the gulp command without any arguments means Gulp will automatically look for the 'default' task and run it.
To run a specific task, simply add the task name to the command. For example, take the following task:

    var gulp = require('gulp');

    gulp.task('foo', function() {
        return console.log('Hello, foo!');
    });

To run it, type the following:

    $ gulp foo

You would then see the following output:

    [20:52:54] Using gulpfile ~/source/gulp-starter/Gulpfile.js
    [20:52:54] Starting 'foo'...
    Hello, foo!
    [20:52:54] Finished 'foo' after 1 ms

## Doing something useful

What makes Gulp different is that it uses node streams. This means tasks can run fast without the need
to write any intermediate files to disk. This is particularly efficient when you want to perform a number of
operations on a set of files.

Take the common task of minification. To concatenate and minify a set of javascript files, you
need to read the directories containing the files, concatenate them into one file then run that file through a
minifier like uglify.

In Gulp this can be achieved through plugins. The above scenario would look like this:

    var gulp = require('gulp'),
        concat = require('gulp-concat'),
        rename = require('gulp-rename'),
        uglify = require('gulp-uglify');

    gulp.task('minifyjs', function() {
        return gulp.src('./src/js/*.js')
            .pipe(concat('combined.js'))
            .pipe(rename({ suffix: '.min' }))
            .pipe(uglify())
            .pipe(gulp.dest('dist/js'));
    });

To perform the above task, the first thing to do is load the required files using the `gulp.src` API method.
This method can accept a glob pattern to match multiple files. The `gulp.src` method returns a node stream
which is then piped using the `.pipe()` method to each plugin in the pipeline. Finally, the `gulp.dest` method
is used to output the contents of the stream to a directory - multiple destinations can be done if needed.

## Plugins

There are thousands of plugins available for Gulp for every conceivable task. It's best to [search the npm
registry](https://www.npmjs.com/search?q=gulp) to see what is available.

It is also worth looking at the Gulp API and having a go at writing your own plugin as well. The beauty of
Gulp is that it is just javascript so you can also just write some glue code to perform tasks as well.

## Take a look at

[Testable GulpJS](/testable-gulp) - writing tests to support your build tooling.