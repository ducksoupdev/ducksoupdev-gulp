---
slug: by-way-of-introduction
title: By way of introduction...
template: post.hbs
author: Imogen Levy
date: 2014-10-13
---

![cake!](/images/cake-1.jpg)

Hi I'm Imogen Levy, the other half of Duck Soup and little sis to Matt!

I am hoping to compliment Matt's more technical focused thinking with thoughts on User Experience, Design, Project Management and how to maximise audience engagement through strategic thinking, solid relationships and a strong desire to succeed (with a bit of geeky code thrown in for good measure!).

I have been in digital development for just over 8 years now.  I have a Masters degree in Interactive Design where I was able to set a grounding in programming by building interactive video-based installations in c++.  From uni I have worked in digital roles for a range of major instiutions including the Church of England, the BBC and presently Westminster Abbey, where I am Digital Manager.

I also make really good cakes!

If you'd like to find out more you can drop me an email at imogen[at]ducksoupdev.co.uk
