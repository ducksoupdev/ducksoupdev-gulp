---
slug: vue-typescript-template
title: Vue Typescript template
template: post.hbs
date: 2017-03-30
author: Matt Levy
tags: vue vuejs typescript
---

![](/images/vue.jpg)

I have started developing apps using Vue.js. It's a lightweight framework with simularities in templating syntax and component set-up to that of Angular so it feels familiar. I am also a big fan of Typescript and wanted a similar set-up with Vue. 

Vue has an excellent CLI for scaffolding apps called [`vue-cli`](https://github.com/vuejs/vue-cli). I was unable to find a template that fitted my needs for Typescript, Webpack and Mocha so I decided to create one.

Luckily, [`vue-cli`](https://github.com/vuejs/vue-cli) is a flexible tool that allows you to scaffold a new app from a template in a Github repo using the repo name, e.g `ducksoupdev/vue-webpack-typescript`. Creating a new template is as straightforward as forking one of the official templates and changing the files to suit your needs.

## vue-webpack-typescript

The template I created is for Vue 2.2, Webpack and Typescript. Out of the box it provides sample components, unit tests and a flexible build for development, testing and production. It comes with hot reload, Mocha unit testing, code coverage with Istanbul, Sass and bundling/minification.

## How to use

Enter the following commands into your terminal to get started with the template.

``` bash
$ npm install -g vue-cli
$ vue init ducksoupdev/vue-webpack-typescript my-project
$ cd my-project
$ npm install
$ npm run dev
```

### What's Included

- `npm run dev`: Webpack + Typescript with proper config for source maps & hot-reload.
- `npm test`: Mocha-based unit tests
- `npm run test:watch`: Mocha-based unit tests with hot-reload
- `npm run coverage`: Karma coverage reporter
- `npm run lint`: Lint all Typescript files
- `npm run build`: build with HTML/CSS/JS minification.
- `npm run ci:teamcity`: Teamcity CI integration
- `npm run ci:jenkins`: Jenkins CI integration

## Feedback welcome

If you like the template, [send me a tweet](https://twitter.com/ducksoupdev) or if you have any suggestions for improving it please [raise an issue of the repo](https://github.com/ducksoupdev/vue-webpack-typescript/issues).
