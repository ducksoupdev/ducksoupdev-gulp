---
slug: i-finally-did-it
title: I finally did it
template: post.hbs
date: 2014-10-07
author: Matt Levy
---

![](/images/better-late-than-never-1.jpg)

So, I finally did it - I started a blog!

Been meaning to do it for years but just not got round to it. So here goes!

I am aiming to blog about my experiences, ideas and thoughts on development.

## About me

I am a web developer. I've been developing for about 15 years - mostly on web technologies. Over the years I have written
websites, applications and tools in a range of technologies and languages - Perl (my first programming language),
Java, Groovy, C#, Javascript.

I am currently working as a senior front-end developer for [Arqiva](http://www.arqiva.com) using the following
technologies: Backbone, AngularJS, NodeJS, Grunt, Gulp, Mocha, Karma, Selenium, Sass, HTML5.

I also do freelance web design and development work so please [get in touch](http://twitter.com/ducksoupdev)!