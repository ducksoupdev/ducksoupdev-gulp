---
slug: building-a-static-site-generator-in-gulp
title: Building a static site generator in Gulp
template: post.hbs
date: 2015-09-08
author: Matt Levy
tags: tooling gulp content static
status: live
---

![](/images/gulp-static.jpg)

Using Gulp to generate a static site.

Having used the Ghost blogging platform for a while, I thought I'd try and create a static site generator for
my site using the same markdown content from Ghost but using [Gulp](http://gulpjs.com).
I looked for a simple Gulp-based one and nothing seem to fit the bill so I had a go at building one.

I'm pleased with the result and am using it currently in place of Ghost. This post looks at how to use it.

## Installation

The generator is [available on Github](https://github.com/ducksoupdev/gulp-site-generator). Add it as a sub-module, use it and
let me know what you think?

    $ mkdir my-static-site
    $ cd my-static-site
    $ git submodule add git@github.com:ducksoupdev/gulp-site-generator.git tools

## What is included?

The generator sits nicely in your site directory structure:

```
gulpfile.js
package.json
site.json
src/
  ├── content/
  |     ├── pages/
  |     ├── posts/
  |     ├── index.md
  |     ├── template.md
  ├── images/
  ├── sass/
  ├── fonts/
  ├── templates/
  ├── favicon.ico
tools/
  ├── gulp/
  |     ├── lib/
  |     ├── tasks/
  |     ├── tests/
```

#### tools/

This directory contains the generator tasks, helpers and tests.

#### src/

The src directory is where the site content is added.

#### src/content/

The content directory contains the posts and pages for the site as markdown files.

#### src/images/

This directory contains the images for the site

#### src/sass/

This directory contains a sample sass file in SCSS format.

#### src/templates/

The templates directory contains the templates used to generate the static HTML files for the site.
[Handlebars](http://handlebarsjs.com/) is used as the template system.

#### site.json

This file contains meta data about the site.

#### gulpfile.js

This is the file gulp uses to run the tasks for the generator.

## How to use

The generator needs to be installed first. An install script is provided and can be run as follows:

    $ node tools/install

Next, the generator has some node dependencies:

    $ npm install 

Finally, you can run the following:

    $ gulp

This fires up the default task which generates the sample site provided. The static site is created
in the `build/` directory.

Pages and posts are merged with their assigned templates, minified and output to their own directory.
For example, `build/my-first-blog-post/index.html`. This creates SEO friendly URLs with links as `/my-first-blog-post/`.

Assets such as images, style sheets, fonts etc are automatically copied to the `build/` directory.

Whenever you add a page, post or static asset, simply run the default task:

    $ gulp

### Options

When creating pages/posts, you can use the `--compile drafts` switch to include draft templates
for reviewing and checking.

    $ gulp --compile drafts

## Viewing the generated site

To view the site, a server task is available:

    $ gulp server

The site will be available at [http://localhost:8080](http://localhost:8080).

## Customisation

You are free to change the generator to fit your needs. It is designed to get you up and running
quickly whilst, at the same time, giving you a starting point for further customisation.

If you encounter any issues or have any ideas for future development, [please let me know](http://twitter.com/ducksoupdev) or [raise an issue](https://github.com/ducksoupdev/gulp-site-generator/issues).

## Creating pages and posts

Pages and posts are markdown files. A template and a couple of sample pages are provided and have the
following content:

```
---
slug: i-am-a-template
title: I am a template
template: post.hbs
date: 2015-02-08
author: Matt Levy
status: draft
---

![](/images/i-am-a-template.jpg)

This is the template full of markdown text!
```

Each markdown file includes YAML front-matter. This is important metadata needed by the generator and includes the following:

* slug (required) - this is the directory name created for the page/post in the `build/` directory
* title (required) - the title of the page/post
* template (required) - the template in `src/templates/` used to create the page
* date (required for posts, optional for pages) - the published date for the post
* author (optional) - the author of the page/post which has meta data in the file `site.json`
* tags (optional) - an optional list of space-separated tags and used to generate an index of tag posts
* status (optional) - an optional status - 'draft' is used for ignoring templates by the generator

### Pages

Pages are designed for general site information such as an 'about' or 'contact us' page.
These appear before posts on the home page.

### Posts

Posts are listed on the home page in descending order by date and include a summary of the post.

Tag pages are generated in the same way as the home page and group posts by tag. These are also listed in
descending order by date.

## Development task

A develop task with live-reload is provided. To use, simply type the following command:

    $ gulp develop --compile drafts

The site will be available to view at [http://localhost:8080](http://localhost:8080).

## Summary

This is a work in progress and I am using it for generating all the pages and posts for this site. If you use
it, let me know what you think or if you find any bugs, [raise them on Github](https://github.com/ducksoupdev/gulp-site-generator/issues).
