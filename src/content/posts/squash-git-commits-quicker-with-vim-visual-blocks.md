---
slug: squash-git-commits-quicker-with-vim-visual-blocks
title: Squash git commits quicker with Vim visual blocks
template: post.hbs
date: 2014-10-14
author: Matt Levy
tags: vim git
---

![](/images/git.jpg)

Using Vim visual blocks will save you having to select each commit one at a time.

Rebasing a series of commits with git from this:

	pick f5gh44h Update readme
    pick 6gu8ss3 Add custom Sass file
    pick 9se4lk1 Update Angular service

to this:

	pick f5gh44h Update readme
    squash 6gu8ss3 Add custom Sass file
    squash 9se4lk1 Update Angular service

Is slow! Look familiar?

## Vim visual blocks

* Move the cursor to the start of the first commit you want to squash
* Hit CTRL_V (Mac, Linux) or CTRL-Q (Windows) to enter visual block mode
* Select all lines containing 'pick'
![](/images/vim-visual-block1.jpg)
* Hit 'c' and type 'squash'
* Hit 'esc' and all 'pick' text will be replaced with 'squash'
![](/images/vim-visual-block2.jpg)