---
slug: angularjs-for-enhancing-web-content
title: AngularJS for enhancing web content
template: post.hbs
date: 2014-10-13
author: Matt Levy
tags: angularjs content dom
---

![](/images/AngularJS_Banner.jpg)

[AngularJS](https://angularjs.org/) is widely used for [SPAs](http://en.wikipedia.org/wiki/Single-page_application) but is not necessarily everyone's first choice for enhancing web content - [jQuery](http://jquery.com) and other libraries are typically used for DOM manipulation.

However, with it's directive API, using AngularJS to enhance web content is a powerful alternative and can be used where [semantic mark-up](http://en.wikipedia.org/wiki/Separation_of_presentation_and_content) and [SEO](http://en.wikipedia.org/wiki/Search_engine_optimization) is important.

I recently did some development for the [Westminster Abbey](http://www.westminster-abbey.org) website. One of the requirements for this mobile-first site was a carousel view for both mobile and desktop which made use of Bootstrap v3 components carousel and collapse.

## Example project available on Github

An example project is [available on Github](https://github.com/ducksoupdev/web-content-angularjs).

First, clone the repo.

	$ git clone https://github.com/ducksoupdev/web-content-angularjs.git

Next, install everything that is required.

	$ cd web-content-angularjs
    $ npm install
    $ bower install


## Building the carousel

The home page of the site has a carousel showing a selection of highlighted content. The following is what the site looks like on both mobile and desktop:

![](/images/wabbey-home-page-carousel.jpg)

The rendered components are collapse (mobile) and carousel (desktop) and are built from the following example markup:

<pre>
&lt;div class=&quot;wa-hero&quot; data-interval=&quot;-1&quot;&gt;
	&lt;section title=&quot;Section 1&quot;&gt;...&lt;/section&gt;
	&lt;section title=&quot;Section 2&quot;&gt;...&lt;/section&gt;
	&lt;section title=&quot;Section 3&quot;&gt;...&lt;/section&gt;
&lt;/div&gt;
</pre>

The **&lt;section&gt;** elements are required for the component to work correctly. However, each section can contain any element - images, video, text etc.

### AngularJS directive

An AngularJS directive, named hero, was created to render the same content in both the collapse and carousel components.

<script src="https://gist.github.com/ducksoupdev/fbdee3b3f523f1f95baa.js"></script>

## Summary

The only limitation is that the directive contains a set of section elements.

The markup does not use any AngularJS specific attributes or elements and is SEO-friendly. It also transforms the existing content to fit the view which enhances the web content for a better user experience.