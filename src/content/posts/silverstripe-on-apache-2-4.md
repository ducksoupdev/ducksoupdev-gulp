---
slug: silverstripe-on-apache-2-4
title: Silverstripe on Apache 2.4
template: post.hbs
date: 2014-10-08
author: Matt Levy
tags: silverstripe apache
---

![](/images/silverstripe-cms1-1.jpg)

I have recently migrated several Silverstripe CMS sites to a new server running Apache v2.4. Access control changed between v2.2 and v2.4.

The following .htaccess files need updating as follows to work with Apache v2.4.

1 - cms/.htaccess

<pre>&lt;FilesMatch "\.(php|php3|php4|php5|phtml|inc)$"&gt;
	Require all denied
&lt;/FilesMatch&gt;
&lt;FilesMatch "silverstripe_version$"&gt;
	Require all denied
&lt;/FilesMatch&gt;</pre>

2 - framework/.htaccess

<pre>&lt;FilesMatch "\.(php|php3|php4|php5|phtml|inc)$"&gt;
	Require all denied
&lt;/FilesMatch&gt;
&lt;FilesMatch "(main|static-main|rpc|tiny_mce_gzip)\.php$"&gt;
	Require all granted
&lt;/FilesMatch&gt;
&lt;FilesMatch "silverstripe_version$"&gt;
	Require all denied
&lt;/FilesMatch&gt;</pre>

3 - .htaccess

<pre>&lt;Files *.ss&gt;
	Require host 127.0.0.1
&lt;/Files&gt;
&lt;Files web.config&gt;
	Require all denied
&lt;/Files&gt;</pre>