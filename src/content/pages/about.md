---
slug: about
title: About Duck Soup
template: page.hbs
---

![](/images/about-us1.jpg)

Duck Soup is the creative partnership of Matt & Imogen Levy. We design and build websites and webapps.

We are friendly and easy to work with.

Looking to hire us? Or need some more information? [Get in touch](https://twitter.com/ducksoupdev)